## Carental

I tried to be innovative in car rental business and created this application to support the concept. It doesn't matter what car you are renting, it matters where the car is in this exact moment you need it. Car will open after you have entered temporary code you got from app. Temporary code is currently just random number generated on the fly in frontend.

## Get started with development

- Install Docker
- Install Docker Compose
- Add `VERIFF_API_KEY` and `VERIFF_API_KEY` in docker.compose.yml

`$ docker-compose up -d`

- App will be running on http://localhost:4000

## Known bugs

- Driver's license country on sign up must be country code like "EE" for example
- Errors from Veriff API end up with 500 for API end user. To see what happened check logs `$ docker logs carental_api`
- If reserving car fails then no error is shown

## Verification on sign up

I know there are other ways but I would like user to have all the steps done in app not redirected somewhere. Veriff integration is basically like the demo: https://github.com/Veriff/js-integration-demo but I never get any decision. Might be because Carental verification doesn't send all the data that Veriff JS demo does.

## Testing

I created some end-to-end tests which will cover almost all the functionality in frontend. Currently it is running on dev env and shares dev database. In real world there should be separate env for that.

Run end-to-end tests

`$ cd ui/`

`$ npm install`

`$ npm run cypress:open`

`

React app has src/shared folder which is only for reusable components and those should be tested separately but they are not covered at the moment.

Running Jest tests

`$ cd ui/`

`$ npm install`

`$ npm run test`

## Authentication

Using just JWT access token which expires in 24 hours. I real world I would add refresh token to renew access token which would expire in quite short amount of time.

## CI

I would use Jenkins but decided not to set it up for now.

## Closing thoughts

I used some new technologies like NestJS in backend and only function components in frontend which use React Hooks to be as capable as class components. It doesn't mean it is right way to go today but I learned a lot doing this test task and continue to develop my skills on that direction. If I missed something or you have questions, please don't hesitate to ask :)