import { Controller, UseGuards, Get, Post, Delete, Param, Logger } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CarsService } from './cars.service';
import { User } from '../auth/auth.decorator';

@Controller('cars')
export class CarsController {
  private readonly logger = new Logger(CarsController.name);

  constructor(private readonly carsService: CarsService) {}

  @Get()
  @UseGuards(AuthGuard())
  async availableCars() {
    return await this.carsService.getAvailableCars();
  }

  @Get('reserved-by-user')
  @UseGuards(AuthGuard())
  async userCars(@User() user) {
    return await this.carsService.getUserCars(user.id);
  }

  @Post(':carId/reservation')
  @UseGuards(AuthGuard())
  async reserve(@Param() params, @User() user) {
    await this.carsService.reserveCar(params.carId, user.id);
  }

  @Delete(':carId/reservation')
  @UseGuards(AuthGuard())
  async cancelReservation(@Param() params, @User() user) {
    await this.carsService.cancelCarReservation(params.carId, user.id);
  }
}
