import { Injectable, Logger, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Car } from './car.entity';

@Injectable()
export class CarsService {
  private readonly logger = new Logger(CarsService.name);

  constructor(
    @InjectRepository(Car)
    private readonly carRepository: Repository<Car>,
  ) {
    this.createCarsIfNeeded();
  }

  async getAvailableCars() {
    const cars = await this.carRepository.createQueryBuilder('car')
      .select(['car.id', 'car.latitude', 'car.longitude'])
      .where('car.reservedBy IS NULL')
      .getMany();

    return cars;
  }

  async getUserCars(userId: number) {
    const cars = await this.carRepository.createQueryBuilder('car')
      .select(['car.id', 'car.latitude', 'car.longitude'])
      .where('car.reservedBy = :userId', { userId })
      .getMany();

    return cars;
  }

  async reserveCar(carId: number, userId: number) {
    const car = await this.carRepository.createQueryBuilder('car')
      .select(['car.id', 'car.reservedBy'])
      .where('car.id = :carId', { carId })
      .getOne();

    if (car.reservedBy) {
      throw new BadRequestException('Car is not available for reservation');
    }

    const cars = await this.carRepository.createQueryBuilder()
      .update('car')
      .set({ reservedBy: userId })
      .where('car.id = :carId', { carId })
      .execute();

    return cars;
  }

  async cancelCarReservation(carId: number, userId: number) {
    const userCar = await this.carRepository.createQueryBuilder('car')
      .select(['car.id'])
      .where('car.id = :carId', { carId })
      .where('car.reservedBy = :userId', { userId })
      .getOne();

    if (!userCar) {
      throw new BadRequestException('User has no reservation on that car');
    }

    await this.carRepository.createQueryBuilder('car')
      .update('car')
      .set({ reservedBy: null })
      .where('id = :carId', { carId })
      .execute();
  }

  async createCarsIfNeeded() {
    const carCount = await this.carRepository.createQueryBuilder('car').getCount();

    if (carCount) {
      return;
    }

    const createCars = [
      {
        latitude: '59.443977',
        longitude: '24.739673',
        reservedBy: null,
      },
      {
        latitude: '59.444539',
        longitude: '24.743112',
        reservedBy: null,
      },
      {
        latitude: '59.430422',
        longitude: '24.745563',
        reservedBy: null,
      },
    ];

    return await this.carRepository.save(
      this.carRepository.create(createCars),
    );
  }
}
