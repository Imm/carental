import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Car {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  longitude: string;

  @Column()
  latitude: string;

  @Column({ nullable: true })
  reservedBy: number;
}
