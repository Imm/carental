import { Injectable, BadRequestException, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { VerifyService } from './verify/verify.service';
import * as crypto from 'crypto';

@Injectable()
export class UserService {
  private readonly logger = new Logger(UserService.name);

  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly verifyService: VerifyService,
  ) {}

  async getByEmail(email: string) {
    const user = await this.userRepository.createQueryBuilder('user')
      .select([
        'user.id',
        'user.email',
        'user.firstName',
        'user.lastName',
        'user.verificationId',
        'user.verificationStatus',
      ])
      .where('user.email = :email')
      .setParameter('email', email)
      .getOne();

    return user;
  }

  async getByEmailAndPassword(email: string, password: string) {
    const passwordHash = crypto.createHmac('sha256', password).digest('hex');
    const user = await this.userRepository.createQueryBuilder('user')
      .select([
        'user.id',
        'user.email',
        'user.firstName',
        'user.lastName',
        'user.verificationId',
        'user.verificationStatus',
      ])
      .where('user.email = :email and user.password = :password')
      .setParameter('email', email)
      .setParameter('password', passwordHash)
      .getOne();

    return user;
  }

  async updateVerificationStatus(userId: number, status: string) {
    await this.userRepository.createQueryBuilder()
      .update('user')
      .set({ verificationStatus: status })
      .where('id = :userId', { userId })
      .execute();
  }

  async create(payload) {
    const user = await this.getByEmail(payload.email);

    if (user) {
      throw new BadRequestException([{
        property: 'email',
        constraints: {
          onlyUniqueEmail: 'a user with provided email already exists',
        },
      }]);
    }

    try {
      const session = await this.verifyService.startSession(payload);
      const verificationId = session.verification.id;

      await Promise.all([
        this.verifyService.uploadImage(verificationId, 'document-front', payload.driversLicenseFrontPhoto),
        this.verifyService.uploadImage(verificationId, 'document-back', payload.driversLicenseBackPhoto),
      ]);

      await this.verifyService.submit(verificationId);

      const createUser = {
        ...payload,
        verificationId,
        verificationStatus: 'verifying',
      };

      return await this.userRepository.save(
        this.userRepository.create(createUser),
      );
    } catch (error) {
      if (error.response) {
        this.logger.error(error.response.data);
      }

      throw error;
    }
  }

  async delete(userId: number) {
    return await this.userRepository.createQueryBuilder()
      .delete()
      .from('user')
      .where('id = :userId', { userId })
      .execute();
  }
}
