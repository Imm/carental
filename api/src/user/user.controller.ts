import { Controller, Get, Post, Delete, UseGuards, Res, Body, Logger } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { UserService } from './user.service';
import { VerifyService } from './verify/verify.service';
import { AuthService } from '../auth/auth.service';
import { User } from '../auth/auth.decorator';
import { UserDto } from './user.dto';

@Controller('user')
export class UserController {
  private readonly logger = new Logger(UserController.name);

  constructor(
    private readonly userService: UserService,
    private readonly verifyService: VerifyService,
    private readonly authService: AuthService,
  ) {}

  @Post('create')
  async create(@Body() payload: UserDto, @Res() response: Response) {
    const user = await this.userService.create(payload);
    const token = this.authService.createToken(user);

    response.cookie('accessToken', token.accessToken, {
      httpOnly: true,
      path: '/',
    });

    return response.json(token);
  }

  @Get('verification-status')
  @UseGuards(AuthGuard())
  async verificationStatus(@User() user: any) {
    const decision = await this.verifyService.getDecision(user.verificationId);

    if (decision.verification) {
      await this.userService.updateVerificationStatus(user.id, decision.verification.status);
    }
  }

  @Delete()
  @UseGuards(AuthGuard())
  async delete(@User() user: any, @Res() response: Response) {
    await this.userService.delete(user.id);
    const token = this.authService.createToken(user);

    response.cookie('accessToken', token.accessToken, {
      httpOnly: true,
      path: '/',
    });

    return response.json(token);
  }
}
