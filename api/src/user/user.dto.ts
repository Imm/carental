import { IsEmail, IsNotEmpty, MinLength } from 'class-validator';

export class UserDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @MinLength(4)
  password: string;

  @IsNotEmpty()
  firstName: string;

  @IsNotEmpty()
  lastName: string;

  @IsNotEmpty()
  idNumber: string;

  @IsNotEmpty()
  driversLicenseNumber: string;

  @IsNotEmpty()
  driversLicenseCountry: string;

  @IsNotEmpty()
  driversLicenseFrontPhoto: string;

  @IsNotEmpty()
  driversLicenseBackPhoto: string;
}
