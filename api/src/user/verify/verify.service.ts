import { Injectable, Logger } from '@nestjs/common';
import axios from 'axios';
import * as crypto from 'crypto';

@Injectable()
export class VerifyService {
  private readonly logger = new Logger(VerifyService.name);

  veriffApiToken: string = process.env.VERIFF_API_TOKEN;
  veriffApiSecret: string = process.env.VERIFF_API_SECRET;
  veriffApiUrl: string = process.env.VERIFF_API_URL;

  async startSession(user) {
    const payload = {
      verification: {
        person: {
          firstName: user.firstName,
          lastName: user.lastName,
          idNumber: user.idNumber,
        },
        document: {
          number: user.driversLicenseNumber,
          type: 'DRIVERS_LICENSE',
          country: user.driversLicenseCountry,
        },
        // additionalData: {
        //   citizenship: user.citizenship,
        //   placeOfResidence: user.placeOfResidence,
        // },
        lang: 'en',
        features: ['selfid'],
        timestamp: this.timestamp(),
      },
    };

    const options = {
      headers: this.getHeaders(payload),
    };

    const response = await axios.post(this.veriffApiUrl + '/sessions', payload, options);
    return response.data;
  }

  async uploadImage(verificationId: string, context: string, image: string) {
    const payload = {
      image: {
        context,
        content: image,
        timestamp: this.timestamp(),
      },
    };

    const options = {
      headers: this.getHeaders(payload),
    };

    const response = await axios.post(this.veriffApiUrl + '/sessions/' + verificationId + '/media', payload, options);
    return response.data;
  }

  async submit(verificationId: string) {
    const payload = {
      verification: {
        frontState: 'done',
        status: 'submitted',
        timestamp: this.timestamp(),
      },
    };

    const options = {
      headers: this.getHeaders(payload),
    };

    const response = await axios.patch(this.veriffApiUrl + '/sessions/' + verificationId, payload, options);
    return response.data;
  }

  async getDecision(verificationId: string) {
    const options = {
      headers: this.getHeaders(verificationId),
    };

    const response = await axios.get(this.veriffApiUrl + '/sessions/' + verificationId + '/decision', options);
    return response.data;
  }

  timestamp() {
    return new Date().toISOString();
  }

  getHeaders(payload) {
    return {
      'x-auth-client': this.veriffApiToken,
      'x-signature': this.generateSignature(payload, this.veriffApiSecret),
      'content-type': 'application/json',
    };
  }

  generateSignature(payload: any, secret: string) {
    if (payload.constructor === Object) {
      payload = JSON.stringify(payload);
    }

    if (payload.constructor !== Buffer) {
      payload = Buffer.from(payload, 'utf8');
    }

    const signature = crypto.createHash('sha256');
    signature.update(payload);
    signature.update(new Buffer(secret, 'utf8'));
    return signature.digest('hex');
  }
}
