import { JwtService } from '@nestjs/jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  createToken(user) {
    const payload: JwtPayload = {
      id: user.id,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      verificationId: user.verificationId,
      verificationStatus: user.verificationStatus,
    };

    return {
      accessToken: this.jwtService.sign(payload),
    };
  }

  async validateUser(payload): Promise<any> {
    const user = await this.userService.getByEmail(payload.email);

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }

  async validateUserPassword(payload): Promise<any> {
    const user = await this.userService.getByEmailAndPassword(payload.email, payload.password);

    if (!user) {
      throw new UnauthorizedException('Email or password not valid');
    }

    return user;
  }
}
