import { Controller, Get, Post, Body, Req, Res, UseGuards, Logger } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response, Request } from 'express';
import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';
import { AuthDto } from './auth.dto';

@Controller('auth')
export class AuthController {
  private readonly logger = new Logger(AuthController.name);

  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Get('status')
  @UseGuards(AuthGuard())
  async status(@Req() request: Request) {
    return {
      accessToken: request.cookies.accessToken,
    };
  }

  @Post('sign-in')
  async signIn(@Body() payload: AuthDto, @Res() response: Response) {
    const user = await this.authService.validateUserPassword(payload);
    const token = this.authService.createToken(user);

    response.cookie('accessToken', token.accessToken, {
      httpOnly: true,
      path: '/',
    });

    return response.json(token);
  }

  @Post('sign-out')
  @UseGuards(AuthGuard())
  async signOut(@Res() response: Response) {
    response.clearCookie('accessToken');
    return response.json();
  }
}
