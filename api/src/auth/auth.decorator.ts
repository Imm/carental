import { createParamDecorator, Logger } from '@nestjs/common';

export const User = createParamDecorator((data, request) => {
  return request.user;
});
