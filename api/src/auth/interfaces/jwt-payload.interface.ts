export interface JwtPayload {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  verificationId: string;
  verificationStatus: string;
}
