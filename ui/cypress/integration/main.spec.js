context('Actions', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4000/cars')

    cy.get('input[placeholder=Email]').type('tester@test.test')
    cy.get('input[placeholder=Password]').type('test')
  })

  it('tries to sign in with wrong password', () => {
    cy.get('input[placeholder=Password]').type('random')

    cy.contains('Sign In').click()
    cy.contains('Email or password not valid')
  })

  it('creates account', () => {
    cy.contains('or sign up').click()

    cy.get('input[placeholder=Email]').type('tester@test.test')
    cy.get('input[placeholder=Password]').type('test')
    cy.get('input[placeholder="First name"]').type('Tes')
    cy.get('input[placeholder="Last name"]').type('Ter')
    cy.get('input[placeholder="ID code"]').type('1234567890')
    cy.get('input[placeholder="Driver\'s license number"]').type('EE123456')
    cy.get('input[placeholder="Driver\'s license country"]').type('EE')

    cy.contains('Take driver\'s license front photo').click()
    cy.wait(500)
    cy.contains('Take a photo').click()

    cy.contains('Take driver\'s license back photo').click()
    cy.wait(500)
    cy.contains('Take a photo').click()

    cy.contains('Sign Up').click()
    cy.contains('Hi Tes').click()
  })

  it('reserves a car and cancels reservation', () => {
    cy.contains('Sign In').click()
    cy.get('area[title=Car]').parent().parent().first().click()
    cy.contains('Reserve the car').click()
    cy.contains('Cancel reservation').click()
  })

  after(() => {
    cy.request('DELETE', 'http://localhost:4001/user')
  })
})
