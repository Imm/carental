import axios from 'axios'

axios.defaults.baseURL = process.env.REACT_APP_API_URL

const submitClientRequest = Symbol('submitClientRequest')

export default class API {
  activeHeaders: {
    'Content-Type': string
  }

  constructor () {
    this.activeHeaders = {
      'Content-Type': 'application/json'
    }
  }

  async [submitClientRequest] (client: any, url: string, payload?: any) {
    const options = {
      headers: this.activeHeaders,
      withCredentials: true
    }

    if (payload) return client(url, payload, options)
    return client(url, options)
  }

  get (url: string, params?: any) {
    return this[submitClientRequest](axios.get, url, params)
  }

  post (url: string, payload?: any) {
    return this[submitClientRequest](axios.post, url, payload)
  }

  patch (url: string, payload?: any) {
    return this[submitClientRequest](axios.patch, url, payload)
  }

  delete (url: string) {
    return this[submitClientRequest](axios.delete, url)
  }

  postFile (url: string, formData: any) {
    return axios.post(url, formData, {
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
  }
}