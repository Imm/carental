import React from 'react'
import styled from 'styled-components'

const About = styled.div`
  padding: 20px;
`

export default () => (
  <About>
    <h4>About Carental</h4>
    Just look at the map, choose a car closest to you and reserve it for as long as you need to use it. To get in the car and drive you need a temporary code which can be generated in our website if you are signed in. Just enter the code to the numeric keypad on the door and drive wherever you need to go. When you are finished close the door and cancel reservation on our website.
  </About>
)