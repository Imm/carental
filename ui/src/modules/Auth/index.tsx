import React, { useState } from 'react'
import styled from 'styled-components'

import { observer } from 'mobx-react-lite'
import { authStore } from '../../store'

import SignUp from '../Auth/SignUp'
import SignIn from '../Auth/SignIn'

const Auth = styled.div`
  padding: 20px;
`

const SignUpLink = styled.button`
  cursor: pointer;
  margin-top: 10px;
  border: none;
  background: none;
  font-size: 14px;
  font-weight: bold;
  outline: none;
  color: SteelBlue;
`

export default observer(() => {
  const [showSignUp, setShowSignUp] = useState(false)

  if (!authStore.isChecked) {
    return null
  }

  return (
    <Auth>
      {showSignUp ? (
        <React.Fragment>
          <h4>To rent a car you need to be registered user and provide photos of your driver's license</h4>
          <SignUp />
          <SignUpLink onClick={() => setShowSignUp(false)}>
            or sign in
          </SignUpLink>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <h4>To rent a car you need to sign in</h4>
          <SignIn />
          <SignUpLink onClick={() => setShowSignUp(true)}>
            or sign up
          </SignUpLink>
        </React.Fragment>
      )}
    </Auth>
  )
})