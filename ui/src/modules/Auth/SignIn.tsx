import React, { useState } from 'react'
import { authStore } from '../../store'
import SignInForm from 'src/shared/Form/SignIn'

export default () => {
  const [errors, setErrors] = useState({})

  const handleSubmit = async (payload: any) => {
    try {
      setErrors({})
      await authStore.signIn(payload)
    } catch(error) {
      if (!error.formError && !error.fieldErrors) {
        throw error
      }

      setErrors(error)
    }
  }

  return (
    <SignInForm onSubmit={handleSubmit} errors={errors} />
  )
}