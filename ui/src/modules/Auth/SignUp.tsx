import React, { useState } from 'react'
import { authStore } from '../../store'
import SignUpForm from 'src/shared/Form/SignUp'
import { IFormErrors } from '../../interfaces'

export default () => {
  const [errors, setErrors] = useState<IFormErrors>()

  const handleSubmit = async (payload: any) => {
    try {
      setErrors({})
      await authStore.signUp(payload)
    } catch(error) {
      if (!error.formError && !error.fieldErrors) {
        throw error
      }

      setErrors(error)
    }
  }

  return (
    <SignUpForm onSubmit={handleSubmit} errors={errors} />
  )
}