import React from 'react'
import styled from 'styled-components'
import Grid from 'src/shared/Grid'
import Button from 'src/shared/Button'
import { observer } from 'mobx-react-lite'
import { ICar } from '../../../interfaces'

const Container = styled.div`
  padding: 20px 0;
`

interface Props {
  car: ICar | null,
  onReserve: Function
}

const SelectedCar: React.SFC<Props> = observer(({ car, onReserve }) => {
  if (!car) {
    return null
  }

  return (
    <Container>
      <Grid container spacing={16}>
        <Grid item xs={12}>
          Selected car ID: {car.id}
        </Grid>
        <Grid item xs={12}>
          <Button onClick={() => onReserve(car)}>Reserve the car</Button>
        </Grid>
      </Grid>
    </Container>
  )
})

export default SelectedCar