import React from 'react'
import styled from 'styled-components'
import { observer } from 'mobx-react-lite'
import { carStore } from '../../../store'
import Button from 'src/shared/Button';

const Container = styled.div`
  padding: 20px 0;
`

const Car = styled.div`
  padding: 5px 0;
  display: flex;
  align-items: center;
`

const CarId = styled.div`
  width: 100px;
`

const CarAccess = styled.div`
  width: 180px;
`

export default observer(() => {
  if (!carStore.userCars || !carStore.userCars.length) {
    return null
  }

  const handleCarAccess = () => {
    alert(`Insert this code on car door to get access: ${Math.floor(Math.random() * 1000000)}\nThe code is valid for 1 minute since generation`)
  }

  return (
    <Container>
      <h4>Your reservations</h4>
      {carStore.userCars.map(car => (
        <Car key={car.id}>
          <CarId>
            Car ID: {car.id}
          </CarId>
          <CarAccess>
            <Button onClick={handleCarAccess}>Get car access</Button>
          </CarAccess>
          <Button onClick={() => carStore.cancelReservation(car.id)}>Cancel reservation</Button>
        </Car>
      ))}
    </Container>
  )
})