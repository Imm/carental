import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { observer } from 'mobx-react-lite'
import { carStore, authStore } from '../../../store'
import Verification from './Verification'
import Map from 'src/shared/Rental/Map'
import SelectedCar from './SelectedCar'
import Reservations from './Reservations'
import { ICar } from '../../../interfaces'

const Reserve = styled.div`
  padding: 20px;
`

export default observer(() => {
  const [selectedCar, setSelectedCar] = useState<ICar | null>(null) 

  useEffect(() => {
    carStore.getAvailableCars()
    carStore.getUserCars()
  }, [])

  const handleCarReserve = async (car: ICar) => {
    await carStore.reserve(car.id)
    setSelectedCar(null)
  }

  return (
    <Reserve>
      <h4>Hi {authStore.user && authStore.user.firstName}</h4>
      <Verification />
      <h4>Select a car you want to rent</h4>
      <Map
        cars={carStore.cars}
        onCarSelect={(car: ICar) => setSelectedCar(car)}
      />
      <SelectedCar car={selectedCar} onReserve={handleCarReserve} />
      <Reservations />
    </Reserve>
  )
})