import React from 'react'
import styled from 'styled-components'
import { observer } from 'mobx-react-lite'
import { authStore } from '../../../store'

const Verification = styled.div`
  padding: 20px 0;
`

const Disclaimer = styled.div`
  margin-top: 10px;
`

export default observer(() => (
  <Verification>
    Your data verification status: {authStore.user && authStore.user.verificationStatus}.
    {authStore.user && authStore.user.verificationStatus !== 'approved' && (
      <Disclaimer>You shouldn't able to rent a car before verification is approved but you can as we are not sure when to expect verification answer or if it ever comes.</Disclaimer>
    )}
  </Verification>
))