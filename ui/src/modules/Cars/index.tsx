import React from 'react'
import styled from 'styled-components'

import { observer } from 'mobx-react-lite'
import { authStore } from '../../store'

import Auth from '../Auth'
import Reserve from './Reserve'

const Cars = styled.div`
  width: 100%;
`

export default observer(() => (
  <Cars>
    {authStore.isAuthenticated ? (
      <Reserve />
    ) : (
      <Auth />
    )}
  </Cars>
))