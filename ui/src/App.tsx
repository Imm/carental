import React from 'react'
import { BrowserRouter, Route, Link } from 'react-router-dom'
import './App.css'

import { observer } from 'mobx-react-lite'
import { authStore } from './store'
import Layout from 'src/shared/Layout'
import Cars from './modules/Cars'
import About from './modules/About'

export default observer(() => {
  let links = [
    <Link to='/cars'>Get a car</Link>,
    <Link to='/'>About</Link>
  ]

  if (authStore.isAuthenticated) {
    links.push(
      <div onClick={() => authStore.signOut()}>Sign out</div>
    )
  }

  return (
    <BrowserRouter>
      <Layout navigation={links}>
        <Route path='/' exact component={About} />
        <Route path='/cars' exact component={Cars} />
      </Layout>
    </BrowserRouter>
  )
})