export interface ICar {
  id: number,
  longitude: string,
  latitude: string
}

export interface IFormErrors {
  formError?: string,
  fieldErrors?: {
    [fieldName: string]: string
  }
}