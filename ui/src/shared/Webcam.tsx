import React, { useRef, FormEvent } from 'react'
import styled from 'styled-components'
import Webcam from 'react-webcam'
import ContainerDimensions from 'react-container-dimensions'
import Button from 'src/shared/Button'
import Grid from 'src/shared/Grid'

const Container = styled.div`
  position: relative;
`

const CapturePhoto = styled.div`
  position: absolute;
  bottom: 5%;
  left: 50%;
  transform: translateX(-50%);
`

interface Props {
  onPhoto: Function,
  name?: string
}

const Webcamera: React.SFC<Props> = ({ onPhoto, name }) => {
  const webcamElement: any = useRef(null)

  const handleCapture = (event: FormEvent) => {
    event.preventDefault()

    if (webcamElement) {
      const image = webcamElement.current.getScreenshot()
      return onPhoto(image, name)
    }
  }

  return (
    <Container>
      <ContainerDimensions>
        {({ width }) => (
          <Webcam
            ref={webcamElement}
            width={width}
            screenshotFormat='image/png'
          />
        )}
      </ContainerDimensions>
      <CapturePhoto>
        <Button onClick={handleCapture}>Take a photo</Button>
      </CapturePhoto>
    </Container>
  )
}

export default Webcamera
