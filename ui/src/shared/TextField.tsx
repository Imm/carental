import styled from 'styled-components'

const TextField = styled.input`
  font-size: 16px;
  padding: 10px 18px;
  width: calc(100% - 40px);
  background: white;
  border: 2px solid SkyBlue;
  border-radius: 20px;
  outline: none;

  :focus {
    border: 2px solid SteelBlue;
  }
`

export default TextField