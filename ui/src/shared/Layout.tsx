import React, { ReactNode } from 'react'
import styled from 'styled-components'

const Header = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: space-between;
  top: 0;
  width: 100%;
  height: 50px;
  background: SkyBlue;
  color: white;
  box-shadow: 0 4px 6px 0px rgba(0, 0, 0, 0.2);
  z-index: 1000;
`

const Logo = styled.div`
  padding: 5px 20px;
  font-size: 30px;
  font-weight: bold;
  letter-spacing: 5px;
  -webkit-text-stroke-width: 1px;
  -webkit-text-stroke-color: lightGray;
`

const Navigation = styled.div`
  display: flex;
  align-items: center;
  margin-right: 20px;
`

const NavItem = styled.div`
  a, div {
    display: flex;
    align-items: center;
    height: 50px;
    padding: 0 20px;
    font-size: 18px;
    color: white;
    text-decoration: none;
    cursor: pointer;

    :hover {
      background: SteelBlue;
    }
  }
`

const Content = styled.div`
  margin: 50px auto 0 auto;
  min-height: calc(100% - 50px);
  max-width: 800px;
  overflow: hidden;
`

interface Props {
  navigation?: ReactNode[]
  children?: any,
}

const Layout: React.SFC<Props> = ({ navigation, children }) => (
  <div>
    <Header>
      <Logo>
        CARENTAL
      </Logo>
      <Navigation>
        {navigation && navigation.map((item, key) => (
          <NavItem key={key}>{item}</NavItem>
        ))}
      </Navigation>
    </Header>
    <Content>
      {children}
    </Content>
  </div>
)

export default Layout
