import React from 'react'
import styled from 'styled-components'
import Grid from 'src/shared/Grid'

const Error = styled.div`
  padding: 5px 0 5px 12px;
  width: 100%;
  color: FireBrick;
`

interface Props {
  error?: string,
  children: any,
  [key: string]: any
}

const Field: React.SFC<Props> = (props) => (
  <Grid item xs={12} {...props}>
    {props.children}
    {props.error && <Error>{props.error}</Error>}
  </Grid>
)

export default Field