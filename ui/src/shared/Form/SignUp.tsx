import React, { useState, FormEvent } from 'react'
import styled from 'styled-components'
import { IFormErrors } from '../../interfaces'
import Grid from 'src/shared/Grid'
import Modal from 'src/shared/Modal'
import Form from 'src/shared/Form'
import FormField from 'src/shared/Form/Field'
import TextField from 'src/shared/TextField'
import Button from 'src/shared/Button'
import Webcam from 'src/shared/Webcam'

const Container = styled.div`
  padding: 0;
`

const LicenseImage = styled.img`
  width: 80%;
  max-width: 300px;
  display: block;
  margin-top: 10px;
`

interface Props {
  onSubmit: Function,
  errors?: IFormErrors
}

const SignUp: React.SFC<Props> = ({ onSubmit, errors }) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [idNumber, setIdNumber] = useState('')
  const [driversLicenseNumber, setDriversLicenseNumber] = useState('')
  const [driversLicenseCountry, setDriversLicenseCountry] = useState('')
  const [driversLicenseFrontPhoto, setDriversLicenseFrontPhoto] = useState(null)
  const [driversLicenseBackPhoto, setDriversLicenseBackPhoto] = useState(null)
  const [webcamModal, setWebcamModal] = useState(false)
  const [photoType, setPhotoType] = useState('')

  const handleSubmit = () => {
    const payload = {
      email,
      password,
      firstName,
      lastName,
      idNumber,
      driversLicenseNumber,
      driversLicenseCountry,
      driversLicenseFrontPhoto,
      driversLicenseBackPhoto
    }

    onSubmit(payload)
  }

  const getError = (fieldName: string): string => {
    if (errors && errors.fieldErrors) {
      return errors.fieldErrors[fieldName]
    } else {
      return ''
    }
  }

  const takeFrontPhoto = (event: FormEvent) => {
    event.preventDefault()
    setPhotoType('document-front')
    setWebcamModal(true)
  }

  const takeBackPhoto = (event: FormEvent) => {
    event.preventDefault()
    setPhotoType('document-back')
    setWebcamModal(true)
  }

  const handlePhoto = (image: string, name: string) => {
    const set: any = {
      'document-front': setDriversLicenseFrontPhoto,
      'document-back': setDriversLicenseBackPhoto
    }

    set[name](image)
    setWebcamModal(false)
  }

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <FormField error={getError('email')}>
          <TextField
            placeholder='Email'
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
        </FormField>
        <FormField error={getError('password')}>
          <TextField
            type='password'
            placeholder='Password'
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
        </FormField>
        <FormField error={getError('firstName')}>
          <TextField
            placeholder='First name'
            value={firstName}
            onChange={e => setFirstName(e.target.value)}
          />
        </FormField>
        <FormField error={getError('lastName')}>
          <TextField
            placeholder='Last name'
            value={lastName}
            onChange={e => setLastName(e.target.value)}
          />
        </FormField>
        <FormField error={getError('idNumber')}>
          <TextField
            placeholder='ID code'
            value={idNumber}
            onChange={e => setIdNumber(e.target.value)}
          />
        </FormField>
        <FormField error={getError('driversLicenseNumber')}>
          <TextField
            placeholder={`Driver's license number`}
            value={driversLicenseNumber}
            onChange={e => setDriversLicenseNumber(e.target.value)}
          />
        </FormField>
        <FormField error={getError('driversLicenseCountry')}>
          <TextField
            placeholder={`Driver's license country`}
            value={driversLicenseCountry}
            onChange={e => setDriversLicenseCountry(e.target.value)}
          />
        </FormField>
        <FormField>
          <Modal
            open={webcamModal}
            onClose={() => setWebcamModal(false)}
          >
            <Webcam
              onPhoto={handlePhoto}
              name={photoType}
            />
          </Modal>
          <Grid container spacing={24}>
            <FormField item xs={6} error={getError('driversLicenseFrontPhoto')}>
              <Button onClick={takeFrontPhoto}>Take driver's license front photo</Button>
              {driversLicenseFrontPhoto && (
                <LicenseImage src={driversLicenseFrontPhoto}/>
              )}
            </FormField>
            <FormField item xs={6} error={getError('driversLicenseBackPhoto')}>
              <Button onClick={takeBackPhoto}>Take driver's license back photo</Button>
              {driversLicenseBackPhoto && (
                <LicenseImage src={driversLicenseBackPhoto}/>
              )}
            </FormField>
          </Grid>
        </FormField>
        <FormField>
          <Button>Sign Up</Button>
        </FormField>
      </Form>
    </Container>
  )
}

export default SignUp