import React, { useState } from 'react'
import styled from 'styled-components'
import { IFormErrors } from '../../interfaces'
import Form from 'src/shared/Form'
import FormField from 'src/shared/Form/Field'
import TextField from 'src/shared/TextField'
import Button from 'src/shared/Button'

const Container = styled.div`
  padding: 0;
`

interface Props {
  onSubmit: Function,
  errors?: IFormErrors
}

const SignUp: React.SFC<Props> = ({ onSubmit, errors }) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const handleSubmit = () => {
    const payload = {
      email,
      password,
    }

    onSubmit(payload)
  }

  const getError = (fieldName: string): string => {
    if (fieldName === 'password' && errors && errors.formError) {
      return errors.formError
    }

    if (errors && errors.fieldErrors) {
      return errors.fieldErrors[fieldName]
    } else {
      return ''
    }
  }

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <FormField error={getError('email')}>
          <TextField
            placeholder='Email'
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
        </FormField>
        <FormField error={getError('password')}>
          <TextField
            type='password'
            placeholder='Password'
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
        </FormField>
        <FormField>
          <Button>Sign In</Button>
        </FormField>
      </Form>
    </Container>
  )
}

export default SignUp