import React, { FormEvent } from 'react'
import Grid from 'src/shared/Grid'

interface Props {
  children?: any
  onSubmit: Function
}

const Form: React.SFC<Props> = ({ children, onSubmit }) => {
  const handleSubmit = (event: FormEvent) => {
    event.preventDefault()
    onSubmit()
  }

  return (
    <form onSubmit={handleSubmit}>
      <Grid container spacing={16}>
        {children}
      </Grid>
    </form>
  )
}

export default Form