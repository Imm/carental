import React from 'react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps'
import { ICar } from '../../interfaces'

interface Props {
  cars: ICar[] | null,
  onCarSelect: Function
}

const Map: React.SFC<Props> = ({ cars, onCarSelect }) => {
  return (
    <GoogleMap
      defaultZoom={13}
      defaultCenter={{ lat: 59.436675, lng: 24.752923 }}
    >
      {cars && cars.map((car) => (
        <Marker
          key={car.id}
          title='Car'
          position={{ lat: Number(car.latitude), lng: Number(car.longitude) }}
          onClick={() => onCarSelect(car)}
        />
      ))}
    </GoogleMap>
  )
}

const MapWithScripts = withScriptjs(withGoogleMap(Map))

const MapContainer: React.SFC<any> = (props) => {
  return (
    <MapWithScripts
      googleMapURL='https://maps.googleapis.com/maps/api/js?key=AIzaSyA2-2ZZFeSgXfGIQPBssQSKF4D5lk-hoA4&v=3.exp&libraries=geometry,drawing,places'
      loadingElement={<div style={{ height: `100%` }} />}
      containerElement={<div style={{ height: `400px` }} />}
      mapElement={<div style={{ height: `100%` }} />}
      {...props}
    />
  )
}

export default MapContainer