import styled from 'styled-components'

const Button = styled.button`
  font-size: 16px;
  padding: 12px 20px;
  background: SkyBlue;
  color: white;
  border-radius: 20px;
  border: 0;
  outline: none;
  cursor: pointer;

  :hover {
    background: SteelBlue;
  }
` 

export default Button