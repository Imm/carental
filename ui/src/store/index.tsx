import ApiClient from '../ApiClient'
import Auth from './Authentication'
import Car from './Car'

export const api = new ApiClient()
export const authStore = new Auth(api)
export const carStore = new Car(api)