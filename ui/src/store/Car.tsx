import { observable, action } from 'mobx'
import ApiClient from '../ApiClient'
import { ICar } from '../interfaces'

export default class Car {
  @observable cars: ICar[] | null = null
  @observable userCars: ICar[] | null = null
  @observable isAuthenticated: boolean = false

  api: ApiClient

  constructor (api: ApiClient) {
    this.api = api
  }

  @action
  async getAvailableCars () {
    const response = await this.api.get(`/cars`)
    this.cars = response.data
    return response.data
  }

  @action
  async getUserCars () {
    const response = await this.api.get(`/cars/reserved-by-user`)
    this.userCars = response.data
    return response.data
  }

  @action
  async reserve (carId: number) {
    const response = await this.api.post(`/cars/${carId}/reservation`, {})
    this.getAvailableCars()
    this.getUserCars()
    return response
  }

  @action
  async cancelReservation (carId: number) {
    const response = await this.api.delete(`/cars/${carId}/reservation`)
    this.getAvailableCars()
    this.getUserCars()
    return response
  }
}
