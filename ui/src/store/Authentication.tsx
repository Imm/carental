import { observable, action } from 'mobx'
import ApiClient from '../ApiClient'

export default class Authentication {
  @observable isChecked: boolean = false
  @observable isAuthenticated: boolean = false
  @observable user: {
    id: number,
    email: string,
    firstName: string,
    lastName: string,
    verificationId: string,
    verificationStatus: string,
  } | null

  api: ApiClient

  constructor (api: ApiClient) {
    this.user = null
    this.api = api
    this.status()
  }

  @action
  async status () {
    try {
      const response = await this.api.get('/auth/status')
      this.userDataFromResponse(response)
      this.isChecked = true
      this.verificationStatus()
    } catch (error) {
      this.isChecked = true
    }
  }

  @action
  async signUp (payload: any) {
    try {
      const response = await this.api.post('/user/create', payload)
      this.userDataFromResponse(response)
    } catch (error) {
      throw this.formErrorAdapter(error)
    }
  }

  @action
  async signIn (payload: any) {
    try {
      const response = await this.api.post('/auth/sign-in', payload)
      this.userDataFromResponse(response)
      this.verificationStatus()
    } catch (error) {
      throw this.formErrorAdapter(error)
    }
  }

  @action
  async signOut () {
    await this.api.post('/auth/sign-out', {})
    this.isAuthenticated = false
    this.user = null
  }

  @action
  async verificationStatus () {
    await this.api.get('/user/verification-status')
  }

  userDataFromResponse (response: any) {
    const token = response.data.accessToken
    const {
      id,
      email,
      firstName,
      lastName,
      verificationId,
      verificationStatus
    } = JSON.parse(window.atob(token.split('.')[1]))

    this.user = {
      id,
      email,
      firstName,
      lastName,
      verificationId,
      verificationStatus
    }

    this.isAuthenticated = true
  }

  formErrorAdapter (error: any) {
    if (!error.response) {
      return error
    }

    const message = error.response.data.message

    let errors: any = {}

    if (Array.isArray(message)) {
      message.forEach((item: {
        constraints: {
          [key: string]: string
        },
        property: string
      }) => {
        let constraints = []

        for (let key in item.constraints) {
          let contraint = item.constraints[key]
          constraints.push(contraint.substr(contraint.indexOf(' ') + 1))
        }

        if (!errors.fieldErrors) {
          errors.fieldErrors = {}
        }

        errors.fieldErrors[item.property] = constraints
      })
    } else if (typeof message === 'string') {
      errors.formError = message
    }

    return errors
  }
}
